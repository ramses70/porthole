#! /usr/bin/env python3

"""
    Porthole dispatcher module
    Holds common debug functions for Porthole

    Fixed for Python 3 - March 2020 Michael Greene

    Copyright (C) 2003 - 2008 Fredrik Arnerup, Brian Dolbec

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

# Fredrik Arnerup <foo@stacken.kth.se>, 2004-12-19
# Brian Dolbec<dol-sen@telus.net>,2005-3-30


from select import select
import os
import sys
import time
import _thread
import threading
import queue
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib
# import inspect
import logging

logger = logging.getLogger(__name__)

class Dispatcher:
    """Send signals from a thread to another thread through a pipe
    in a thread-safe manner
    """

    def __init__(self, callback_func, *args, **kwargs):
        self.callback = callback_func
        self.callback_args = args
        self.callback_kwargs = kwargs
        self.continue_io_watch = True

        # Queues - https://docs.python.org/3.6/library/asyncio-queue.html
        self.queue = queue.Queue(maxsize=0)  # thread safe queue
        # https://docs.python.org/3.6/library/os.html?pipe#os.pipe
        self.pipe_r, self.pipe_w = os.pipe()

        """
        glib.io_add_watch(fd, condition, callback, ...)
        fd monitored by the main loop for condition:
            glib.IO_IN      There is data to read.
            glib.IO_OUT     Data can be written (without blocking).
            glib.IO_PRI     There is urgent data to read.
            glib.IO_ERR     Error condition.
            glib.IO_HUP     Hung up (the connection has been broken, usually for pipes and sockets).
        
        Additional arguments to pass to callback can be specified after callback. The idle priority 
        may be specified as a keyword-value pair with the keyword "priority". The signature of the 
        callback function is:

            def callback(source, cb_condition, ...)
        where source is fd, the file descriptor; cb_condition is the condition that triggered the signal; 
        and, ... are the zero or more arguments that were passed to the glib.io_add_watch() function.

        If the callback function returns False it will be automatically removed from the list of event 
        sources and will not be called again. If it returns True it will be called again when the 
        condition is matched.
        """
        GLib.io_add_watch(self.pipe_r, GLib.IO_IN, self.on_data)

    def __call__(self, *args):
        """Emit signal from thread"""
        # PLaces args in the queue
        self.queue.put(args)
        # write to pipe afterwards --- is this to signal queue msg waiting? MKG
        os.write(self.pipe_w, "X".encode())

    def on_data(self, source, cb_condition):
        if select([self.pipe_r], [], [], 0)[0] and os.read(self.pipe_r, 1):
            if self.callback_args:
                args = self.callback_args + self.queue.get()
                self.callback(*args, **self.callback_kwargs)
            else:
                self.callback(*self.queue.get(), **self.callback_kwargs)
        return self.continue_io_watch


class Dispatch_wait:
    """Send signals from a thread to another thread through a pipe
    in a thread-safe manner. wait for data to return"""

    def __init__(self, callback_func, *args, **kwargs):
        self.callback = callback_func
        self.callback_args = args
        self.callback_kwargs = kwargs
        self.continue_io_watch = True
        self.callQueue = queue.Queue(0)  # thread safe queue
        self.reply = queue.Queue(0)
        self.callpipe_r, self.callpipe_w = os.pipe()
        self.wait = {}  # dict of boolleans for incoming thread id's waiting for replies
        self.Semaphore = threading.Semaphore()
        GLib.io_add_watch(self.callpipe_r, GLib.IO_IN, self.on_calldata)

    def __call__(self, *args):  # this function is running in the calling thread
        """
        Get thread ID, put it in the queue, write to pipe  to signal
        Emit signal from thread
        """
        logger.debug("DISPATCHER: in Dispatch_wait __call__")
        self.semsig = self.Semaphore
        id = _thread.get_ident()
        self.Queue.put([args, id])
        # write to pipe afterwards
        os.write(self.callpipe_w, "X".encode())
        # now wait for the reply
        self.semsig.acquire()
        logger.debug("DISPATCHER: Sem-On")
        self.wait[id] = True
        self.semsig.release()
        logger.debug("DISPATCHER: Sem-Off")
        while self.wait[myid]:
            # pass the time waiting for a reply by having a snooze
            time.sleep(0.01)
        myreply.reply_id = self.reply.get()
        if reply_id != id:
            logger.debug("DISPATCH_WAIT:  Uh-Oh! id's do not match!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", file=sys.stderr)
        return myreply

    def on_calldata(self, source, cb_condition):
        if select([self.callpipe_r], [], [], 0)[0] and os.read(self.callpipe_r, 1):
            args, id = self.Queue.get()
            if self.callback_args:
                reply = self.callback(*(self.callback_args + args), **self.callback_kwargs)
            else:
                reply = self.callback(*args, **self.callback_kwargs)
            self.Semaphore.aquire()
            self.reply.put([reply, id])
            self.wait[id] = False
            self.Semaphore.release()
        return self.continue_io_watch
