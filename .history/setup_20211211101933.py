#!/usr/bin/env python3

import porthole.version as Version
from setuptools import setup

datadir = "share/porthole/"

setup(name="porthole",
      version=Version.get_version(),
      url="https://gitlab.com/mikeos2/porthole/-/wikis/Porthole3",
      description="GTK+ frontend to Portage",
      author="Michael Greene, Fredrik Arnerup, Daniel G. Taylor, Brian Dolbec, William F. Wheeler",
      author_email="mikeos2@gmail.com, dol-sen@users.sourceforge.net, ""farnerup@users.sourceforge.net, dgt84@users.sourceforge.net, "" tiredoldcoder@users.sourceforge.net",
      license="GNU GENERAL PUBLIC LICENSE, Version 2",
      packages=['porthole', 'porthole.advancedemerge', 'porthole.backends', 'porthole.config',
                'porthole.db', 'porthole.dialogs', 'porthole.loaders', 'porthole.packagebook',
                'porthole.loaders', 'porthole.readers', 'porthole.terminal',
                'porthole.utils', 'porthole.views', 'porthole._xml'],
      package_dir={'porthole': 'porthole'},
      scripts=["scripts/porthole"],
      data_files=[
          (datadir + "pixmaps",
           ["porthole/pixmaps/porthole-about.png", "porthole/pixmaps/porthole.png",
            "porthole/pixmaps/porthole-clock-20x20.png", "porthole/pixmaps/porthole-clock.png",
            "porthole/pixmaps/porthole.svg"]),
          (datadir + "help",
           ["porthole/help/advemerge.html", "porthole/help/advemerge.png", "porthole/help/changelog.png",
            "porthole/help/custcmd.html", "porthole/help/custcmd.png", "porthole/help/customize.html",
            "porthole/help/dependencies.png", "porthole/help/index.html", "porthole/help/install.html",
            "porthole/help/installedfiles.png", "porthole/help/mainwindow.html", "porthole/help/mainwindow.png",
            "porthole/help/porthole.css", "porthole/help/queuetab.png", "porthole/help/search.html",
            "porthole/help/summarytab.png", "porthole/help/sync.html", "porthole/help/termrefs.html",
            "porthole/help/termwindow.html", "porthole/help/termwindow.png", "porthole/help/toc.html",
            "porthole/help/unmerge.html", "porthole/help/update.html", "porthole/help/warningtab.png",
            "porthole/help/depview.png", "porthole/help/ebuildtable_explained2.png",
            "porthole/help/upgradeables.png"]),
          (datadir + "glade",
           ["porthole/glade/about.glade", "porthole/glade/advemerge.glade",
            "porthole/glade/config.glade", "porthole/glade/main_window.glade",
            "porthole/glade/process_window.glade",
            "porthole/glade/run_dialog.glade"]),
          (datadir,
           ["scripts/dopot.sh", "scripts/pocompile.sh", "AUTHORS"]),
          (datadir + "config",
           ["porthole/config/configuration.xml"]),
          (datadir + "i18n",
           ["porthole/i18n/messages.pot", "porthole/i18n/TRANSLATING"]),
          ("share/applications", ["porthole.desktop"]),
          ("share/pixmaps", ["porthole/pixmaps/porthole.png"])
      ], install_requires=['PyGObject', 'portage']
      )
