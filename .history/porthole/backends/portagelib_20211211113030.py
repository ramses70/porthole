#!/usr/bin/env python3

"""
    PortageLib
    An interface library to Gentoo's Portage

    Fixed for Python 3 - March 2020 Michael Greene

    Copyright (C) 2003 - 2009 Fredrik Arnerup, Daniel G. Taylor,
    Wm. F. Wheeler, Brian Dolbec, Tommy Iorns

    Copyright(c) 2004, Karl Trygve Kalleberg <karltk@gentoo.org>
    Copyright(c) 2004, Gentoo Foundation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

__all__ = (
    'PortageSettings'
)

import os
from os import path
import _thread
from gettext import gettext as _
import datetime
import logging
from porthole import config
from porthole.utils.utils import is_root
from porthole.utils.dispatcher import Dispatcher
from porthole.utils.dispatcher import Dispatch_wait
from porthole.backends.properties import Properties
from porthole.backends.utilities import read_bash
from porthole.backends.metadata import parse_metadata
from porthole.sterminal import SimpleTerminal

logger = logging.getLogger(__name__)
logger.debug("PORTAGELIB: id initialized to %d", datetime.datetime.now().microsecond)

"""
The original code had routines for checking for for Portage 2.1 or => 2.2 not much use 
considering 2.3.69 is the low end in portage as of today. 
"""
try:
    import portage
except ModuleNotFoundError:
    exit(_('Could not find portage module. Are you sure this is a Gentoo system?'))

import portage
import portage.const as portage_const
import portage.manifest as manifest
from _emerge.actions import load_emerge_config
from portage.versions import catpkgsplit as catpkgsplit
from portage.versions import catsplit as catsplit
from portage.versions import best
from portage.versions import pkgsplit as portpkgsplit
from portage.package.ebuild.getmaskingstatus import getmaskingstatus
from portage.package.ebuild.getmaskingreason import getmaskingreason
# from portage.package.ebuild.config.config import reload
from portage.util import grabfile

"""
catpkgsplit(cpv_string)
Takes a complete catpkg-version string, and splits into category, package name, 
package version, package revision. Will return None if argument string is invalid 
-- such as missing a version, or category.

catsplit(catpkg_string)
Takes a string in catpkg or catpkg-version format, and splits into category and 
"other" part (list of length 2) at the initial "/" separator. Equivalent to 
string.split(catpkg_string, "/", maxsplit=1). No validation of the package or 
package-version part is performed.

pkgsplit(pv_or_cpv_string)	
Takes a complete catpkg-version string or a pkg-version string, and splits into 
catpkg (or package, if pkg-version specified), package version, package revision. 
Will return None if argument string is invalid -- such as missing a version, or category.
"""

# Note: settings = PortageSettings() to ease settings.XXXX in reading the module

logger.debug("PORTAGELIB: portage version = %s", portage.VERSION)

# thread_id = os.getpid()
thread_id = _thread.get_ident()
logger.debug("PORTAGELIB: Thread ID %s", thread_id)

# only statement executed before setting up PortageSettings()
if is_root():  # then import some modules and run it directly
    import porthole.backends.set_config

"""
Function list
    The functions mark with * are callable from other threads. See end of this module.

    -- make.conf functions
    def get_make_conf(want_linelist=False, savecopy=False)
    def set_make_conf(property, add='', remove='', replace='', callback=None)
    
    * def get_virtuals()
    def reload_portage()
    def get_sets_list(filename)
    * def split_atom_pkg(pkg)
    def get_use_flag_dict(portdir)
    * def get_portage_environ(var)  # examples: DISTDIR, PORTAGE_CONFIGROOT, PORTAGE_TMPDIR, PORTDIR
    def get_arch()
    def get_cpv_use(cpv)
    def get_name(full_name)
    def pkgsplit(ebuild)                        --> wrapper for pkgsplit
    def get_category(full_name)
    def get_full_name(ebuild)
    * def get_installed(package_name)
    * def xmatch(*args, **kwargs)
    def get_version(ebuild)
    * def get_versions(full_name, include_masked=True)
    * def get_hard_masked(full_name)
    def extract_package(ebuild)
    def get_installed_files(ebuild)    
    * def get_property(ebuild, property)  ## this is obsolete
    def best(versions)
    * def get_best_ebuild(full_name)
    * def get_dep_ebuild(dep)
    def get_archlist()
    def get_masking_status(ebuild)              --> wrapper for getmaskingstatus
    def get_masking_reason(ebuild)              --> wrapper for getmaskingreason
    * def get_size(mycpv)
    def get_digest(ebuild):  ## depricated
    * def get_properties(ebuild)
    * def get_virtual_dep(atom)
    def is_overlay(cpv)
    def get_overlay(cpv)
    def get_overlay_name(ovl)
    * def get_path(cpv)
    def get_metadata(package)
    * def get_system_pkgs()
    * def find_best_match(search_key)
    def split_package_name(name)
    def get_allnodes()
    * def get_installed_list()
    def get_installed_ebuild_path(fullname)
"""


def get_make_conf(want_linelist=False, savecopy=False):
    """
    **** I cannnot find a caller for this function ***

    Parses /etc/make.conf into a dictionary of items with
    dict[setting] = properties string

    If want_linelist is True, the list of lines read from make.conf will also
    be returned.

    If savecopy is true, a copy of make.conf is saved in make.conf.bak.

    TODO: check for root if savecopy true - fail or bypass+alert if no permissions
    """
    logger.debug("PORTAGELIB: get_make_conf()")
    makeconf_path = config.Paths.get_makeconf_path()
    file = open(makeconf_path, 'r')
    if savecopy:
        bakfile = makeconf_path + '.bak'
        file2 = open(bakfile, 'w')
        file2.write(file.read())
        file.close()
        file2.close()
        return True
    lines = file.readlines()
    file.close()
    linelist = []
    for line in lines:
        strippedline = line.strip()
        if strippedline.startswith('#'):
            linelist.append([strippedline])
        elif '=' in strippedline:
            splitline = strippedline.split('=', 1)
            if '"' in splitline[0] or "'" in splitline[0]:
                logger.debug(" * PORTAGELIB: get_make_conf(): couldn't handle line '%s'. Ignoring", line)
                linelist.append([strippedline])
            else:
                linelist.append(splitline)
            # linelist.append([splitline[0]])
            # linelist[-1].append('='.join(splitline[1:])) # might have been another '='
        else:
            logger.debug(" * PORTAGELIB: get_make_conf(): couldn't handle line '%s'. Ignoring", line)
            linelist.append([strippedline])
    make_dict = {}
    for line in linelist:
        if len(line) == 2:
            make_dict[line[0]] = line[1].strip('"')  # line[1] should be of form '"settings"'
    if want_linelist:
        return make_dict, linelist
    return make_dict


def set_make_conf(makeconf_property, add='', remove='', replace='', callback=None):
    """
    Called from AdvancedEmergeDialog - on_make_conf_commit which is a button callback
    from AdvancedEmerge Dialog

    Sets a variable in make.conf.
    If remove: removes elements of <remove> from variable string.
    If add: adds elements of <add> to variable string.
    If replace: replaces entire variable string with <replace>.

    if remove contains the variable name, the whole variable is removed.

    e.g. set_make_conf('USE', add=['gtk', 'gtk2'], remove=['-gtk', '-gtk2'])
    e.g. set_make_conf('ACCEPT_KEYWORDS', remove='ACCEPT_KEYWORDS')
    e.g. set_make_conf('PORTAGE_NICENESS', replace='15')
    """
    logger.debug("PORTAGELIB: set_make_conf()")
    file = 'make.conf'
    if isinstance(add, list):
        add = ' '.join(add)
    if isinstance(remove, list):
        remove = ' '.join(remove)
    if isinstance(replace, list):
        replace = ' '.join(replace)
    if not os.access(config.Paths.get_makeconf_path(), os.W_OK):
        command = (config.Prefs.globals.su + ' "python ' + config.Prefs.DATA_PATH
                   + 'backends/set_config.py -d -f %s ' % file)
        command = (command + '-p %s ' % makeconf_property)
        if add != '':
            command = (command + '-a %s ' % ("'" + add + "'"))
        if remove != '':
            command = (command + '-r %s' % ("'" + remove + "'"))
        command = command + '"'
        logger.debug(" * PORTAGELIB: set_make_conf(); command = %s", command)
        if not callback:
            callback = reload_portage
        app = SimpleTerminal(command, False, dprint_output='SET_MAKE_CONF CHILD APP: ', callback=Dispatcher(callback))
        app._run()
    else:
        add = add.split()
        remove = remove.split()
        set_config.set_make_conf(makeconf_property, add, remove, replace)
        if callback:
            callback()
        else:
            reload_portage()
    # This is slow, but otherwise portage doesn't notice the change.
    # reload_portage()
    # Note: could perhaps just update portage.settings.
    # portage.settings.pmaskdict, punmaskdict, pkeywordsdict, pusedict
    # or portage.portdb.settings ?
    return True


def get_virtuals():
    # returns all virtuals same as porttree
    logger.debug('PORTAGELIB: get_virtuals no user identified')
    return settings.settings.virtuals


def reload_portage():
    """
    I just could not find reload --- check later
    """
    logger.debug('PORTAGELIB: reloading portage BUG BUG BUG')
    logger.debug("PORTAGELIB: old portage version = %s", portage.VERSION)
    # reload(portage)
    logger.debug("PORTAGELIB: BUG-BUG WTF is the reload(portage) coming from????")
    logger.debug("PORTAGELIB: new portage version = %s", portage.VERSION)
    settings.reset()


def get_sets_list(filename):
    """Get the package list file and turn it into a tuple"""
    # attributes: pkgs[key = full_name] = [atoms, version]
    logger.debug('PORTAGELIB: get_sets_list no user identified BUG BUG BUG')
    pkgs = {}
    try:
        linelist = read_bash(filename)
    except Warning:
        logger.debug("PORTAGELIB: get_sets_list(); Failure to locate file: %s", filename)
        return None
    # split the atoms from the pkg name and any trailing attributes if any
    for item in linelist:
        parts = split_atom_pkg(item)
        pkgs[parts[0]] = parts[1:]
    return pkgs


"""
    ebuild ATOM: A depend atom is simply a dependency that is used by portage when calculating 
    relationships between packages.  Please note that if the atom has not already been emerged, 
    then the latest version available is matched.

    - Atom Bases
    The base atom is just a full category/packagename.
    - Atom Versions
    It is nice to be more specific and say that only certain versions of atoms are acceptable.
    -Atom Prefix Operators [> >= = <= <]
    Sometimes you want to be able to depend on general versions rather than specifying exact 
    versions all the time.
    - Extended Atom Prefixes [!~] and Postfixes [*]
    Now to get even fancier, we provide the ability to define blocking packages and version 
    range matching.
    - Atom Slots
    Beginning with EAPI 1, any atom can be constrained to match a specific SLOT.
"""


def split_atom_pkg(pkg):
    """Extract [category/package, atoms, version] from some ebuild identifier

    input:  >=app-admin/packagekit-qt-0.9.5
    output: ['app-admin/packagekit-qt', '>=', '0.9.5']
    """
    atoms = []
    version = ''
    ver_suffix = ''
    if pkg.endswith("*"):
        pkg = pkg[:-1]
        ver_suffix = '*'
    # handle atom prefix
    while pkg[0] in ["<", ">", "=", "!", "*"]:
        atoms.append(pkg[0])
        pkg = pkg[1:]
    cplist = catpkgsplit(pkg) or catsplit(pkg)
    if not cplist or len(cplist) < 2:
        logger.debug("PORTAGELIB ERROR split_atom_pkg(): issues with '%s'", pkg)
        return ['', '', '']
    cp = cplist[0] + "/" + cplist[1]
    if cplist:
        if len(cplist) > 2:
            version = cplist[2] + ver_suffix
        if len(cplist) > 3 and cplist[3] != 'r0':
            version += '-' + cplist[3]
    # print([str(cp), ''.join(atoms), version])
    return [str(cp), ''.join(atoms), version]  # hmm ... unicode keeps appearing :(


def get_portage_environ(var):
    """Returns environment variable from portage if possible, else None"""
    # examples: DISTDIR, PORTAGE_CONFIGROOT, PORTAGE_TMPDIR, PORTDIR
    # pretty straight forward
    # in:  "PORTDIR"
    # out: /usr/portage
    try:
        # temp = portage_config().environ()[var]
        temp = settings.settings.environ()[var]
    except ValueError:
        temp = None
    return temp


def get_arch():
    """Return host CPU architecture"""
    # out:  amd64   (in this case intel 64)
    return settings.settings["ARCH"]


def get_cpv_use(cpv):
    """uses portage to determine final USE flags and settings for an emerge"""
    logger.debug("PORTAGELIB: get_cpv_use(); cpv = %s", cpv)
    settings.settings.unlock()
    settings.settings.setcpv(cpv, use_cache=True, mydb=settings.portdb)
    myuse = settings.settings['PORTAGE_USE'].split()
    logger.debug("PORTAGELIB: get_cpv_use(); type(myuse), myuse = %s   %s", str(type(myuse)), str(myuse))
    # use_expand_hidden =  settings.settings._get_implicit_iuse()
    use_expand_hidden = settings.settings["USE_EXPAND_HIDDEN"].split()
    logger.debug(
        "PORTAGELIB: get_cpv_use(); type(use_expand_hidden), use_expand_hidden = %s   %s", str(type(use_expand_hidden)),
        str(use_expand_hidden))
    usemask = list(settings.settings.usemask)
    useforce = list(settings.settings.useforce)
    # reset cpv filter
    settings.settings.reset()
    settings.settings.lock()
    return myuse, use_expand_hidden, usemask, useforce


# sort of - returns first name between "/" and end
def get_name(full_name):
    """Extract name from full name."""
    # in:  "/usr/src/linux"
    # out: usr
    if type(full_name) is str:
        return full_name.split('/')[1]
    if full_name is None:
        print("WTF??? get_name portlib fullname None")
        return
    if isinstance(full_name, 'porthole.db.package.Package'):
        print("class")
    print("WTF??? get_name portlib fullname %s", full_name)
    return full_name.split('/')[1]


# noinspection PyUnresolvedReferences
def pkgsplit(ebuild):
    """Split ebuild into [category/package, version, revision]"""
    # pkgsplit --> wrapper for pkgsplit
    # in:  sys-kernel/gentoo-sources-5.5.6-r1
    # out: ('sys-kernel/gentoo-sources', '5.5.6', 'r1')
    return portpkgsplit(ebuild)  # portage.versions.pkgsplit()


def get_category(full_name):
    """Extract category from full name."""
    # in:  sys-kernel/gentoo-sources-5.5.6-r1
    # out: sys-kernel
    return full_name.split('/')[0]


def get_full_name(ebuild):
    """Extract category/package from some ebuild identifier"""
    # in:  sys-kernel/gentoo-sources-5.5.6-r1
    # out: sys-kernel/gentoo-sources
    return split_atom_pkg(ebuild)[0]


def get_installed(package_name):
    """Extract installed versions from package_name."""
    # package_name can be the short package name ('eric'), long package name ('dev-util/eric')
    # or a version-matching string ('>=dev-util/eric-2.5.1:2[flag1 flag2]')
    # in:  clang
    # out: ['sys-devel/clang-7.1.0', 'sys-devel/clang-8.0.1', 'sys-devel/clang-10.0.0']
    return settings.trees[settings.settings["ROOT"]]["vartree"].dep_match(str(package_name))


def xmatch(*args, **kwargs):
    """Pass arguments on to portage's caching match function.
    xmatch('match-all',package-name) returns all ebuilds of <package-name> in a list,
    xmatch('match-visible',package-name) returns non-masked ebuilds,
    xmatch('match-list',package-name,mylist=list) checks for <package-name> in <list>
    There are more possible arguments.
    package-name may be, for example:
       gnome-base/control-center            ebuilds for gnome-base/control-center
       control-center                       ebuilds for gnome-base/control-center
       >=gnome-base/control-center-2.8.2    only ebuilds with version >= 2.8.2


    match mode	        Description	                                                            Return Type
    bestmatch-visible	Find the best (highest) visible (unmasked) match for the dependency.	single string
    match-all	        Find all matches, masked or unmasked.	                                list of strings
    match-visible	    Find all visible (unmasked) matches for the dependency.	                list of strings
    minimum-all	        Find the lowest match for the dependency, ignoring masks.	            single string
    minimum-visible	    Find the lowest match for the dependency, respecting masks.	            single string

    """
    results = settings.portdb.xmatch(*args, **kwargs)[:]  # make a copy.  needed for <portage-svn-r5382
    # print >>stderr, type(results), str(results)
    return results


def get_version(ebuild):
    """Extract version number from ebuild name"""
    # in:  sys-kernel/gentoo-sources-5.5.6-r1
    # out: 5.5.6-r1
    result = ''
    parts = catpkgsplit(ebuild)
    if parts:
        result = parts[2]
        if parts[3] != 'r0':
            result += '-' + parts[3]
    return result


def get_versions(full_name, include_masked=True):
    """Returns all available ebuilds for the package"""
    # Note: this is slow, especially when include_masked is false
    # in:  sys-kernel/gentoo-sources
    # out: [ --all matching packages-- ]
    criterion = include_masked and 'match-all' or 'match-visible'
    v = xmatch(criterion, str(full_name))
    return v


def get_hard_masked(full_name):
    full_name = str(full_name)
    hardmasked = []
    try:  # newer portage
        pmaskdict = settings.portdb.settings.pmaskdict[full_name]
    except AttributeError:  # older portage
        try:
            pmaskdict = settings.portdb.mysettings.pmaskdict[full_name]
        except KeyError:
            pmaskdict = {}
    except KeyError:
        pmaskdict = {}
    for x in pmaskdict:
        m = xmatch("match-all", x)
        for n in m:
            if n not in hardmasked:
                hardmasked.append(n)
    hard_masked_nocheck = hardmasked[:]
    try:  # newer portage
        punmaskdict = settings.portdb.settings.punmaskdict[full_name]
    except AttributeError:  # older portage
        try:
            punmaskdict = settings.portdb.mysettings.punmaskdict[full_name]
        except KeyError:
            punmaskdict = {}
    except KeyError:
        punmaskdict = {}
    for x in punmaskdict:
        m = xmatch("match-all", x)
        for n in m:
            while n in hardmasked:
                hardmasked.remove(n)
    return hard_masked_nocheck, hardmasked


def extract_package(ebuild):
    """Returns cat/package from cat/package-ebuild,
       or None if input is not in that format.  """
    result = None
    parts = catpkgsplit(ebuild)
    if parts:
        result = "/".join(parts[0:2])
    return result


# this is obsolete
def get_property(ebuild, inproperty):
    """Read a property of an ebuild. Returns a string."""
    # portage.auxdbkeys contains a list of properties
    if settings.portdb.cpv_exists(ebuild):  # if in portage tree
        return settings.portdb.aux_get(ebuild, [inproperty])[0]
    else:
        vartree = settings.trees[settings.settings["ROOT"]]["vartree"]
        if vartree.dbapi.cpv_exists(ebuild):  # elif in installed pkg tree
            return vartree.dbapi.aux_get(ebuild, [inproperty])[0]
        else:
            return ''


def best(versions):
    """returns the best version in the list"""
    # Pretty easy - return best version in passed list
    # in example:
    #   - get_full_name("sys-kernel/gentoo-sources-5.5.6-r1") --> sys-kernel/gentoo-sources
    #   - get_versions( result of above ) --> list of all kernels
    #   - best( result of above - list of all kernels )
    # out: given example data above - sys-kernel/gentoo-sources-5.6.11
    # noinspection PyUnresolvedReferences
    return portage.best(versions)


def get_best_ebuild(full_name):
    return xmatch("bestmatch-visible", str(full_name))  # no unicode


def get_dep_ebuild(dep):
    """progresively checks for available ebuilds that match the dependency.
    returns what it finds as up to three options."""
    # dprint("PORTAGELIB: get_dep_ebuild(); dep = " + dep)
    keyworded_ebuild = masked_ebuild = ''
    best_ebuild = xmatch("bestmatch-visible", dep)
    if best_ebuild == '':
        # dprint("PORTAGELIB: get_dep_ebuild(); checking masked packages")
        full_name = split_atom_pkg(dep)[0]
        hardmasked_nocheck, hardmasked = get_hard_masked(full_name)
        matches = xmatch("match-all", dep)[:]
        masked_ebuild = best(matches)
        for m in matches:
            if m in hardmasked:
                matches.remove(m)
        keyworded_ebuild = best(matches)
    # dprint("PORTAGELIB: get_dep_ebuild(); ebuilds = " + str([best_ebuild, keyworded_ebuild, masked_ebuild]))
    return best_ebuild, keyworded_ebuild, masked_ebuild


def get_archlist():
    """lists the architectures accepted by portage as valid keywords"""
    return settings.settings["PORTAGE_ARCHLIST"].split()
    # ~ list = portage.archlist[:]
    # ~ for entry in list:
    # ~ if entry.startswith("~"):
    # ~ list.remove(entry)
    # ~ return list


"""
get_masking_status --> wrapper for getmaskingstatus
"""


def get_masking_status(ebuild):
    # porthole-0.6.1-masking_status.patch
    # return portage.getmaskingstatus(ebuild)
    try:
        status = getmaskingstatus(ebuild)
    except KeyError:
        status = ['deprecated']
    return status


"""
get_masking_reason --> wrapper for getmaskingreason
"""


def get_masking_reason(ebuild):
    """Strips trailing \n from, and returns the masking reason given by portage"""
    reason, location = getmaskingreason(ebuild, settings=settings.settings, portdb=settings.portdb,
                                        return_location=True)
    if not reason:
        reason = _('No masking reason given.')
        status = get_masking_status(ebuild)
        if 'profile' in status:
            reason = _("Masked by the current profile.")
            status.remove('profile')
        if status:
            reason += " from " + ', '.join(status)
    if location is not None:
        reason += "in file: " + location
    if reason.endswith("\n"):
        reason = reason[:-1]
    return reason


def get_size(mycpv):
    """ Returns size of package to fetch. """
    # This code to calculate size of downloaded files was taken from /usr/bin/emerge - BB
    # new code chunks from emerge since the files/digest is no longer, info now in Manifest.
    # dprint( "PORTAGELIB: get_size; mycpv = " + mycpv)
    fetchlist = None
    num = 0
    mysum = [0, '']
    myebuild = settings.portdb.findname(mycpv)
    pkgdir = os.path.dirname(myebuild)
    mf = manifest.Manifest(pkgdir, settings.settings["DISTDIR"])
    final_use, use_expand_hidden, usemasked, useforced = get_cpv_use(mycpv)
    # dprint( "PORTAGELIB: get_size; Attempting to get fetchlist final use= " + str(final_use))
    try:
        fetchlist = settings.portdb.getFetchMap(mycpv, set(final_use))
        num = convert_bytes(mf.getDistfilesSize(fetchlist))
    except KeyError as e:
        mysum[1] = "Unknown (missing digest)"
        logger.debug("PORTAGELIB: get_size; Exception: %s", str(e))
        logger.debug("PORTAGELIB: get_size; ebuild: %s", str(mycpv))
        logger.debug("PORTAGELIB: get_size; fetchlist = %s", str(fetchlist))
    return num


def convert_bytes(num):
    """
    this function will convert bytes to MB.... GB... etc
    """
    step_unit = 1000.0  # 1024 bad the size

    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if num < step_unit:
            return "%3.1f %s" % (num, x)
        num /= step_unit


def get_digest(ebuild):  # # depricated
    """Returns digest of an ebuild"""
    mydigest = settings.portdb.finddigest(ebuild)
    digest_file = []
    if mydigest != "":
        try:
            myfile = open(mydigest, "r")
            for line in myfile.readlines():
                digest_file.append(line.split(" "))
            myfile.close()
        except SystemExit:
            raise  # Needed else can't exit
        except Exception as e:
            logger.debug("PORTAGELIB: get_digest(): Exception: %s", e)
    return digest_file


def get_properties(ebuild):
    """Get all ebuild variables in one chunk."""
    ebuild = str(ebuild)  # just in case -- yeah, but with py3 really needed? MKG
    if settings.portdb.cpv_exists(ebuild):  # if in portage tree
        # The following statement fills the debug log, be careful
        # logger.debug("PORTAGELIB: get_properties(): in tree %s", ebuild)
        try:
            return Properties(dict(zip(settings.keys, settings.portdb.aux_get(ebuild, portage.auxdbkeys))))
        except IOError as e:  # Sync being performed may delete files
            logger.debug(" * PORTAGELIB: get_properties(): IOError: %s", str(e))
            return Properties()
        except Exception as e:
            logger.debug(" * PORTAGELIB: get_properties(): Exception: %s", str(e))
            return Properties()
    else:
        logger.debug("PORTAGELIB: get_properties(): in alt %s", ebuild)
        vartree = settings.trees[settings.settings["ROOT"]]["vartree"]
        if vartree.dbapi.cpv_exists(ebuild):  # elif in installed pkg tree
            return Properties(dict(zip(settings.keys, vartree.dbapi.aux_get(ebuild, portage.auxdbkeys))))
        else:
            return Properties()


def get_virtual_dep(atom):
    """
    ToDo This needs more checking, I cannot find dep_virtual in portage, so for now
    pop out a debug message and return atom - MKG

    returns a resolved virtual dependency.
    contributed by Jason Stubbs, with a little adaptation"""
    # Thanks Jason
    logger.debug("PORTAGELIB: in get_virtual_dep possible bug!!!!")
    # non_virtual_atom = portage.dep_virtual([atom], settings.settings)[0]
    # if atom == non_virtual_atom:
    #     # atom,"is a 'new style' virtual (aka regular package)"
    #     return atom
    # else:
    #     # atom,"is an 'old style' virtual that resolves to:  non_virtual_atom
    #     return non_virtual_atom
    return atom


def is_overlay(cpv):  # lifted from gentoolkit
    """Returns true if the package is in an overlay."""
    try:
        ovl_dir, ovl = settings.portdb.findname2(cpv)
    except ValueError:
        return False
    return ovl != settings.portdir


def get_overlay(cpv):
    """Returns an overlay."""
    if '/' not in cpv:
        return ''
    try:
        dir_overlay, ovl = settings.portdb.findname2(cpv)
    except ValueError:
        ovl = 'Depricated?'
    return ovl


def get_overlay_name(ovl):
    try:  # new upcoming portage function
        name = settings.portdb.getRepositoryName(ovl)
    except EnvironmentError:  # Fallback method
        name = settings.repos.get(ovl)
    return name or "????"


def get_path(cpv):
    """Returns a path to the specified category/package-version"""
    if '/' not in cpv:
        return ''
    try:
        ovl_dir, ovl = settings.portdb.findname2(cpv)
    except ValueError:
        ovl_dir = ''
    return ovl_dir


def get_metadata(package):
    """Get the metadata for a package"""
    # we could check the overlay as well,
    # but we are unlikely to find any metadata files there
    meta_data_file = settings.portdir + "/" + package + "/metadata.xml"

    try:
        if path.isfile(meta_data_file):
            return parse_metadata(meta_data_file)
    except ValueError:
        return None
    # ToDo finish overlay
    try:
        pkg_cpv = find_best_match(package)
        if not is_overlay(pkg_cpv):
            return None
        print(get_path(pkg_cpv))
        return None
    except ValueError:
        return None

    # try:
    #     return parse_metadata(settings.portdir + "/" + package + "/metadata.xml")
    # except ValueError:
    #     print(is_overlay(package))
    #    return None


def get_system_pkgs():  # lifted from gentoolkit
    """Returns a tuple of lists, first list is resolved system packages,
    second is a list of unresolved packages.
    Return list of core (system) packages. Core packages are usually
    consider vital for basic system operativity.
    """
    resolved = []
    unresolved = []
    packages = portage.settings.packages
    for x in packages:
        cpv = x.strip()
        if len(cpv) and cpv[0] == "*":
            pkg = find_best_match(cpv[1:])
            if pkg:
                resolved.append(get_full_name(pkg))
            else:
                unresolved.append(get_full_name(cpv))
    return resolved + unresolved


def find_best_match(search_key):  # lifted from gentoolkit and updated
    """Returns a Package object for the best available installed candidate that
    matched the search key. Doesn't handle virtuals perfectly"""
    # FIXME: How should we handle versioned virtuals??
    # cat,pkg,ver,rev = split_package_name(search_key)
    full_name = split_atom_pkg(search_key)[0]
    if "virtual" == get_category(full_name):
        # t= get_virtual_dep(search_key)
        t = settings.trees[settings.settings["ROOT"]]["vartree"].dep_bestmatch(full_name)
    else:
        t = settings.trees[settings.settings["ROOT"]]["vartree"].dep_bestmatch(search_key)
    if t:
        # dprint("PORTAGELIB: find_best_match(search_key)=" + search_key + " ==> " + str(t))
        return t
    logger.debug("PORTAGELIB: find_best_match(search_key)= %s None Found", search_key)
    return None


def split_package_name(name):  # lifted from gentoolkit, handles vituals for find_best_match()
    """Returns a list on the form [category, name, version, revision]. Revision will
    be 'r0' if none can be inferred. Category and version will be empty, if none can
    be inferred."""
    logger.debug(" * PORTAGELIB: split_package_name() name = %s", name)
    r = catpkgsplit(name)
    if not r:
        r = name.split("/")
        if len(r) == 1:
            return ["", name, "", "r0"]
        else:
            return r + ["", "r0"]
    if r[0] == 'null':
        r[0] = ''
    return r


def get_allnodes():
    """"porttree contains every ebuild """
    return settings.trees[settings.settings["ROOT"]]['porttree'].getallnodes()[:]  # copy


def get_installed_list():
    """"vartree contains every installed ebuild """
    return settings.trees[settings.settings["ROOT"]]["vartree"].getallnodes()[:]  # try copying...


def get_installed_ebuild_path(fullname):
    return settings.trees[settings.settings["ROOT"]]["vartree"].getebuildpath(fullname)


# gentoolkit/helpers.py - mkg
#
# noinspection PyUnresolvedReferences
def get_porttree():
    """"porttree contains every ebuild """
    return portage.db[portage.root]["porttree"].dbapi.cp_all(sort=True)[:]


# noinspection PyUnresolvedReferences
def get_vartree():
    """"vartree contains every installed ebuild """
    return portage.db[portage.root]["vartree"].dbapi.cp_all(sort=True)[:]


# noinspection PyUnresolvedReferences
def get_bintree():
    """Get all binary packages available."""
    return portage.db[portage.root]["bintree"].dbapi.cp_all(sort=True)[:]


# virtuals = portage.db[portage.root]["virtuals"]
# noinspection PyUnresolvedReferences
def get_installed_ebuildpath(fullname):
    return portage.db[portage.root]["vartree"].dbapi.getebuildpath(fullname)


class PortageSettings:
    def __init__(self):
        # declare some globals
        self.portdir = None
        self.portdir_overlay = None
        self.ACCEPT_KEYWORDS = None
        self.user_config_dir = None
        self.config_root = None
        self.trees = None
        self.settings = None
        self.mtimedb = None
        self.world = None  # list of all world packages - loaded by reload_world
        self.SystemUseFlags = None
        self.portdb = None
        self.virtuals = None
        self.keys = None
        self.UseFlagDict = None
        self.repos = None
        self.world = []
        self.system = []
        self.reset()

    def reset(self):
        """reset remaining run once variables after a sync or other mods"""
        logger.debug("PORTAGELIB: PortageSettings: reset_globals()")
        # these calls pull in alot of info, too much to list here
        self.settings, self.trees, self.mtimedb = load_emerge_config()
        self.portdb = self.trees[self.settings["ROOT"]]["porttree"].dbapi
        self.portdir = self.settings.environ()['PORTDIR']
        self.config_root = self.settings['PORTAGE_CONFIGROOT']
        # is PORTDIR_OVERLAY always defined?
        self.portdir_overlay = self.settings.environ()['PORTDIR_OVERLAY']
        # noinspection PyBroadException
        try:
            # ToDo is this good, does exist in settings?
            self.ACCEPT_KEYWORDS = self.settings.environ()['ACCEPT_KEYWORDS']
        except:
            pass
        # self.ACCEPT_KEYWORDS = get_portage_environ('ACCEPT_KEYWORDS')
        self.user_config_dir = portage_const.USER_CONFIG_PATH
        logger.debug("PORTAGELIB: Portdb              %s", self.portdb)
        logger.debug("PORTAGELIB: PORTDIR             %s", self.portdir)
        logger.debug("PORTAGELIB: PORTAGE_CONFIGROOT  %s", self.config_root)
        logger.debug("PORTAGELIB: PORTDIR_OVERLAY     %s", self.portdir_overlay)
        logger.debug("PORTAGELIB: ACCEPT_KEYWORDS     %s", self.ACCEPT_KEYWORDS)
        logger.debug("PORTAGELIB: USER_CONFIG_PATH    %s", self.user_config_dir)
        config.Paths.set_makeconf_path(os.path.join(self.config_root, self.user_config_dir, "make.conf"))
        self.reload_world()
        self.reset_use_flags()
        self.virtuals = self.settings.virtuals
        # lower case is nicer
        self.keys = [key.lower() for key in portage.auxdbkeys]
        self.UseFlagDict = self.get_use_flag_dict(self.portdir)
        self.create_repos()
        return

    """ *************************************************************************************
    reload_world()

    Builds a list of files that makeup world for the running system 

    The world set, also referred to as @world in Portage development, encompasses the system set and 
    the selected set. Packages belonging to the world set are listed by default into the 
    /var/lib/portage/world file. It is the world file, together with the profile and /etc/portage 
    (including make.conf) settings, that defines what software is installed on a system. Later, when 
    a world update is requested (through emerge -uDN @world or similar command), Portage will use 
    the world file as the base for its update calculations.

    Old code had /var/cache/edb/world check, obsolete mid-2000s so removed
    """

    def reload_world(self):
        _world = None
        logger.debug("PORTAGELIB: reading world")
        self.world = []
        try:
            file = open(os.path.join('/', portage.WORLD_FILE), "r")  # "/var/lib/portage/world", "r")
            _world = file.read().split()  # read in as words
            file.close()
        except FileNotFoundError:
            logger.debug("PORTAGELIB: get_world(); Failed to locate the world file")
        self.world = _world

    def set_system(self, system_files):
        """ get files in system set """
        self.system = system_files

    # noinspection PyUnresolvedReferences
    def reset_use_flags(self):
        logger.debug("PORTAGELIB: PortageSettings: Settings.reset_use_flags();")
        self.SystemUseFlags = portage.settings["USE"].split()
        logger.debug("PORTAGELIB: Settings.reset_use_flags(); SystemUseFlags = %s", str(self.SystemUseFlags))

    def get_use_flag_dict(self, portdir):
        """ Get all the use flags and return them as a dictionary
            key = use flag forced to lowercase
            data = list[0] = 'local' or 'global'
                   list[1] = 'package-name'
                list[2] = description of flag
        """
        use_flag_dict = {}

        # process standard use flags
        use_list = grabfile(self.portdir + '/profiles/use.desc')
        for item in use_list:
            index = item.find(' - ')
            use_flag_dict[item[:index].strip().lower()] = ['global', '', item[index + 3:]]

        # process local (package specific) use flags
        use_list = grabfile(portdir + '/profiles/use.local.desc')
        for item in use_list:
            index = item.find(' - ')
            data = item[:index].lower().split(':')
            use_flag_dict[data[1].strip()] = ['local', data[0].strip(), item[index + 3:]]
        return use_flag_dict

    def create_repos(self):
        # reverse the treemap's key:data for easy name lookup
        # until the new getRepositoryName() is fully available.
        logger.debug("PORTAGELIB: PortageSettings: create_repos()")
        t = self.portdb.treemap
        n = {}
        for x in t.keys():
            n[t[x]] = x
        self.repos = n

    def reload_config(self):
        """Reload the whole config from scratch"""
        logger.debug("PORTAGELIB: PortageSettings: reload_config()")
        self.settings, self.trees, self.mtimedb = load_emerge_config(self.trees)
        self.portdb = self.trees[self.settings["ROOT"]]["porttree"].dbapi
        self.create_repos()

    def get_world(self):
        """ Return list of files that makeup world for the running system """
        return self.world

    def get_system(self):
        """ Return list of files that makeup system for the running system """
        return self.system


""" ********************************************************************************* """
logger.debug("PORTAGELIB: PortageSettings: define as settings")
settings = PortageSettings()
logger.debug("PORTAGELIB: PortageSettings: settings defined")

# ToDo set the system files for something I am thinking about doing later
# not pretty - but what is?
settings.set_system(get_system_pkgs())

func = {'get_virtuals': get_virtuals,
        'split_atom_pkg': split_atom_pkg,
        'get_portage_environ': get_portage_environ,
        'get_installed': get_installed,
        'xmatch': xmatch,
        'get_versions': get_versions,
        'get_hard_masked': get_hard_masked,
        'get_property': get_property,
        'get_best_ebuild': get_best_ebuild,
        'get_dep_ebuild': get_dep_ebuild,
        'get_size': get_size,
        'get_properties': get_properties,
        'get_virtual_dep': get_virtual_dep,
        'get_path': get_path,
        'get_system_pkgs': get_system_pkgs,
        'find_best_match': find_best_match,
        'get_installed_list': get_installed_list,
        }


def call_waiting(*args, **kwargs):
    """function to handle function calls from other
        threads in this thread and retun results
        Parameters: args = [function-name, parameter1,parameter2,...]
    """
    if kwargs:
        print(kwargs)
    call = func[args[0]]
    arg = args[1:]
    reply = call(*arg)
    return reply


# create our thread call dispatcher
dispatch_wait = Dispatch_wait(call_waiting)

# # debug code follows WFW
# #polibkeys = UseFlagDict.keys()
# #polibkeys.sort()
# #for polibkey in polibkeys:
# #    print polibkey, ':', UseFlagDict[polibkey]
