#!/usr/bin/env python3

"""
    Porthole version
    Copyright (C) 2006 -2010 Brian Dolbec

    Fixed for Python 3 - March 2020 Michael Greene

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import sys
from gettext import gettext as _

version = "0.7.0_alpha"  # corvid-19 version
_copyright = _("Copyright (c) 2003 - 2020")


def get_version():
    return version


def get_copyright():
    return _copyright


# ********** Main Entry **********
#
def main():
    print(get_version())
    print(get_copyright)
    return


if __name__ == "__main__":
    # execute only if run as a script
    sys.exit(main())
